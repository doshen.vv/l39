package ru.inordic.jart.l38;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.inordic.jart.l28.NordicSpringAppInitializer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

/**
 * @author Artem R. Romanenko
 * @version 16.10.2021
 */
public class MainAppTest {

    @Test
    void testSpringWithTomcat() throws Exception {
        int port = 8081;
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8081);
        Path apssbaseDir = Files.createTempDirectory("tomcatBase");
        Path tomcatDoc = Files.createTempDirectory("tomcatDoc");
        String baseDir = apssbaseDir.toAbsolutePath().toString();
        tomcat.setBaseDir(baseDir);
        tomcat.getConnector();
        Context context = tomcat.addContext("", tomcatDoc.toFile().getAbsolutePath());
        context.addServletContainerInitializer(new NordicSpringAppInitializer(), Collections.emptySet());
        tomcat.start();
        {
            Document state1 = Jsoup.connect("http://localhost:8081").get();
            Elements trs = state1.select(".mainTable tr");
            Assertions.assertEquals(1, trs.size());
        }

        byte[] avaBytes;
        try (InputStream avaRes = getClass().getResourceAsStream("ava.png")) {
            avaBytes = IOUtils.toByteArray(avaRes);
        }

        Document doc = Jsoup.connect("http://localhost:" + port + "/create")
                .data("snils", "2")
                .data("surname", "3")
                .data("name", "4")
                .data("age", "55")
                .data("ava", "test.png", new ByteArrayInputStream(avaBytes), "image/png")
                .cookie("JSESSIONID", "ijasiodjioas")
                .post();
        {
            Document state2 = Jsoup.connect("http://localhost:8081").get();
            Elements trs = state2.select(".mainTable tr");
            Assertions.assertEquals(2, trs.size());
        }

        tomcat.stop();
    }

}
