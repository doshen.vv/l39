package ru.inordic.jart.l30;

import org.junit.jupiter.api.Test;

/**
 * @author Artem R. Romanenko
 * @version 12.09.2021
 */
public class FirstMissingPositive {


    /**
     * Given an unsorted integer array nums, return the smallest missing positive integer.
     * <p>
     * You must implement an algorithm that runs in O(n) time and uses constant extra space.
     * Example 1:
     * <p>
     * Input: nums = [1,2,0]
     * Output: 3
     * <p>
     * Example 2:
     * <p>
     * Input: nums = [3,4,-1,1]
     * Output: 2
     * <p>
     * Example 3:
     * <p>
     * Input: nums = [7,8,9,11,12]
     * Output: 1
     * <p>
     * Constraints:
     * <p>
     * 1 <= nums.length <= 5 * 105
     * -2^31 <= nums[i] <= 2^31 - 1
     */
    @Test
    void test() {
        int sum = 0;
        for (int i = 0; i <= 12; i++) {
            sum += i;
        }
        System.out.println(sum);
        //int_max = 100
        //101*50 = 5050

        //2 3 7 9 5 6 11 12 1

        //max=12 sum(1,12) 78
        //rel_sum()=56
        //22 - сумма пропущенных

        //колебания между соседями
        //1 4 2 -4 1 5 1  -11     // sum() = -1



    }


}








