package ru.inordic.jart.l29;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author Artem R. Romanenko
 * @version 06.09.2021
 */
public class ContextParameterTest {

    @Test
    void test() {
//        StorageProvider.setFilename("asb");
        ApplicationContext ctx
                = new AnnotationConfigApplicationContext("ru.inordic.jart.l29") {
            @Override
            protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
                super.postProcessBeanFactory(beanFactory);
                beanFactory.registerSingleton("storageProvider", new StorageProvider("asb"));
            }
        };
        Assertions.assertTrue(ctx instanceof AnnotationConfigApplicationContext);
//        Assertions.assertEquals(AnnotationConfigApplicationContext.class,ctx.getClass());

        ctx.getBean(Dao.class);

    }

}
