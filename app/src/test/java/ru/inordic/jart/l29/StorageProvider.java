package ru.inordic.jart.l29;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Artem R. Romanenko
 * @version 06.09.2021
 */

public class StorageProvider {

    private String filename;

//    public static void setFilename(String filename) {
//        StorageProvider.filename = filename;
//    }


    public StorageProvider(String filename) {
        this.filename = filename;
    }
//
//    @Autowired
//    public StorageProvider() {
////        this.filename = filename;
//    }

    public String getFilename(){
        return filename;
    }
}
