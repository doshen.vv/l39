package ru.inordic.jart.l36;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.inordic.jart.l28.service.HumanService;
import ru.inordic.jart.l28.web.WebServer;

public class CreateAndViewTest {


    @Test
    void test() throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("ru.inordic.jart.l28");
        WebServer ws = context.getBean(WebServer.class);
        {
            {
                Document state1 = Jsoup.connect("http://localhost:8080").get();
                Elements trs = state1.select(".mainTable tr");
                Assertions.assertEquals(1, trs.size());
            }
            Document doc = Jsoup.connect("http://localhost:8080/create")
                    .requestBody("snils=2&surname=3&name=4&age=55")
                    .cookie("JSESSIONID", "ijasiodjioas")
                    .post();
            {
                Document state2 = Jsoup.connect("http://localhost:8080").get();
                Elements trs = state2.select(".mainTable tr");
                Assertions.assertEquals(2, trs.size());
            }
        }
        ws.forceStop();
        context.close();
    }
}
