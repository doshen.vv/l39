package ru.inordic.jart.l35;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TryMustacheTest {


    @Test
    void test() throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("name", "Mustache");

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader("Hello world {{name}}"), "example");
        mustache.execute(writer, params);
        writer.flush();
        String result = writer.toString();

        Assertions.assertEquals("Hello world Mustache", result);
        System.out.println("result " + result);
    }

    static class Params {
        private boolean printHello;
        private String name;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isPrintHello() {
            return printHello;
        }

        public void setPrintHello(boolean printHello) {
            this.printHello = printHello;
        }
    }

    @Test
    void test2() throws Exception {
        Params params = new Params();
        params.setName("Java");
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader("Hello world {{name}}"), "example");
        mustache.execute(writer, params);
        writer.flush();
        String result = writer.toString();
        Assertions.assertEquals("Hello world Java", result);
        System.out.println("result " + result);
    }

    @Test
    void test3() throws Exception {
        {
            Params params = new Params();
            params.setName("Java");
            params.setPrintHello(false);
            Writer writer = new StringWriter();
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(new StringReader("{{#printHello}}Hello world{{/printHello}} {{name}}"), "example");
            mustache.execute(writer, params);
            writer.flush();
            String result = writer.toString();
            Assertions.assertEquals(" Java", result);
            System.out.println("result " + result);
        }
        {
            Params params = new Params();
            params.setName("Java");
            params.setPrintHello(true);
            Writer writer = new StringWriter();
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(new StringReader("{{#printHello}}Hello world{{/printHello}} {{name}}"), "example");
            mustache.execute(writer, params);
            writer.flush();
            String result = writer.toString();
            Assertions.assertEquals("Hello world Java", result);
            System.out.println("result " + result);
        }
    }

    @Test
    void test4() throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("name", "Mustache");
        Params embedParams = new Params();
        embedParams.setName("Java");
        Params embedParams2 = new Params();
        embedParams2.setName("Kotlin");


        params.put("embedParams", Arrays.asList(embedParams, embedParams2));
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader(
                "Hello world {{name}} " +
                        "{{#embedParams}} is {{name}} {{/embedParams}}"), "example");

        Writer writer = new StringWriter();
        mustache.execute(writer, params);
        writer.flush();
        String result = writer.toString();
        Assertions.assertEquals("Hello world Mustache  is Java  is Kotlin ", result);
    }
}
