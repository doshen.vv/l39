package ru.inordic.jart.jsoup;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class JsoupTest {

    @Test
    void test1() throws Exception {
        InputStream rs = JsoupTest.class.getResourceAsStream("example.html");
        String content = IOUtils.toString(rs, "UTF-8");
        Document doc = Jsoup.parse(content);
        Elements ourPgmLinks = doc.select("div a");

        Assertions.assertEquals(2, ourPgmLinks.size());
        Map<String, String> expLinks = new HashMap<>();
        expLinks.put("Jsoup", "https://jsoup.org");
        expLinks.put("Wikipedia", "https://wikipedia.org");

        Map<String, String> realLinks = new HashMap<>();

        for (Element el : ourPgmLinks) {
            String href = el.attr("href");
            String label = el.text();
            realLinks.put(label, href);
        }

        Assertions.assertEquals(expLinks, realLinks);

    }
}
