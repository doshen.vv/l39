package ru.inordic.jart.l38;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import ru.inordic.jart.l28.NordicSpringAppInitializer;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

/**
 * @author Artem R. Romanenko
 * @version 16.10.2021
 */
public class TomcatRunner {
    public static void main(String[] args) throws Exception{
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8081);
        Path apssbaseDir = Files.createTempDirectory("tomcatBase");
        Path tomcatDoc = Files.createTempDirectory("tomcatDoc");
        String baseDir = apssbaseDir.toAbsolutePath().toString();
        tomcat.setBaseDir(baseDir);
        tomcat.getConnector();
        Context context = tomcat.addContext("", tomcatDoc.toFile().getAbsolutePath());
        context.addServletContainerInitializer(new NordicSpringAppInitializer(), Collections.emptySet());
        tomcat.start();
    }
}
