package ru.inordic.jart.l28.service;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.inordic.jart.l28.event.MyLogEvent;

/**
 * @author Artem R. Romanenko
 * @version 12.09.2021
 */
@Component
public class NrdListener {

    private int countOperations = 0;

    @EventListener

    public void onLog(MyLogEvent event) {
        countOperations++;
        System.out.println("MyLogEvent " + event);
    }

    public int getCountOperations() {
        return countOperations;
    }

    public void increment() {
        countOperations++;
        System.out.println("increment");
    }


    @EventListener
    public void onApplicationEvent(ContextClosedEvent event) {
        System.out.println("=====CONTEXT STARTED. SPARTAA!!!!!!!!!!!!");
    }
}
