package ru.inordic.jart.l28;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

public class NordicSpringAppInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {

        AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
        ac.scan("ru.inordic.jart.l28");
        ctx.addListener(new ContextLoaderListener(ac));
        ServletRegistration.Dynamic d = ctx.addServlet("dispatcher-servlet", new DispatcherServlet(ac));
        d.addMapping("/*");
        d.setLoadOnStartup(1);
        Path tmp;
        try {
            tmp = Files.createTempDirectory("mf");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(tmp.toAbsolutePath().toString(),
                1_000_000, 1_000_000, 1_000_000);
        d.setMultipartConfig(multipartConfigElement);

    }
}
