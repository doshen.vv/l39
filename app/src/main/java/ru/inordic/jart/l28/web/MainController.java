package ru.inordic.jart.l28.web;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.inordic.jart.l28.pojo.Human;
import ru.inordic.jart.l28.pojo.HumanDto;
import ru.inordic.jart.l28.service.HumanService;


import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Artem R. Romanenko
 * @version 16.10.2021
 */

@Controller
public class MainController {
    @Autowired
    private HumanService humanService;

    @GetMapping("/")
    String root(Model map) throws Exception {
        List<Human> peoples = humanService.getAll();
        map.addAttribute("allPeople", peoples.size());
        List<HumanDto> dtoList = peoples.stream().map(new Function<Human, HumanDto>() {
            @Override
            public HumanDto apply(Human h) {
                HumanDto dto = new HumanDto();
                dto.setAge(h.getAge());
                dto.setName(h.getName());
                dto.setSurname(h.getSurname());
                dto.setSnils(h.getSnils());
                dto.setImgAvatar("data:image/png;base64, " + Base64.encodeBase64String(h.getAvatar()));
                return dto;
            }
        }).collect(Collectors.toList());
        map.addAttribute("peoples", dtoList);

        return "allHtmlExample";
    }

    @GetMapping("/create")
    String formForCreate() throws Exception {
        return "create";//-> /templates/create.template.html
    }

    @PostMapping("/create")
    String create(@RequestParam("snils") String snils,
                  @RequestParam("name") String name,
                  @RequestParam("surname") String surname,
                  @RequestParam("age") int age,
                  @RequestParam("ava") MultipartFile ava) throws Exception {
        byte[] avaBytes = ava.getBytes();
        humanService.createNew(snils, name,
                surname, age, avaBytes);
        return "successful";
    }
}
