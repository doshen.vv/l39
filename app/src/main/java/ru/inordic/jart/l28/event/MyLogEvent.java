package ru.inordic.jart.l28.event;

import org.springframework.context.ApplicationEvent;

import java.time.Clock;

/**
 * @author Artem R. Romanenko
 * @version 12.09.2021
 */

public class MyLogEvent extends ApplicationEvent {
    public MyLogEvent(Object source) {
        super(source);
    }

}
