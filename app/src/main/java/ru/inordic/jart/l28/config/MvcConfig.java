package ru.inordic.jart.l28.config;

import com.github.mjeanroy.springmvc.view.mustache.configuration.EnableMustache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * @author Artem R. Romanenko
 * @version 16.10.2021
 */
@EnableMustache
@Configuration
public class MvcConfig {

    //    @Bean(name = "multipartResolver")
//    public CommonsMultipartResolver multipartResolver() {
//        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//        multipartResolver.setMaxUploadSize(1_000_000);
//        return multipartResolver;
//    }
    @Bean
    public StandardServletMultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
}
