package ru.inordic.jart.l28.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import ru.inordic.jart.l28.MyQual;
import ru.inordic.jart.l28.dao.HumanDao;
import ru.inordic.jart.l28.dao.HumanDaoImpl;
import ru.inordic.jart.l28.service.HumanService;
import ru.inordic.jart.l28.service.HumanServiceImpl;

/**
 * @author Artem R. Romanenko
 * @version 01.09.2021
 */
@Configuration
@EnableAsync
public class MyConfig {

    @Bean
//    @MyQual
    public HumanDao humanDao24() {
        System.out.println("-----------create humanDao");
        return new HumanDaoImpl();
    }

//    @Bean
//    public HumanService humanService(@MyQual HumanDao humanDao) {
//        System.out.println("-----------create humanService");
//        HumanServiceImpl hs = new HumanServiceImpl();
//        hs.init(humanDao);
//        return hs;
//    }


}
