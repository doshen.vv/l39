package ru.inordic.jart.l28.mvc;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import com.github.mustachejava.MustacheResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.inordic.jart.l28.web.WebServer;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

@Configuration
public class MustacheConfig {

    @Bean
    MustacheFactory mustacheFactory(){
        MustacheResolver mr  = new MustacheResolver() {
            @Override
            public Reader getReader(String resourceName) {
                Reader r=null;
                switch (resourceName){
                    case "rowTemplate":
                        try {
                            r=new InputStreamReader(WebServer.class.getResourceAsStream("rowTemplate.html"),"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }
                        break;
//                    default:
//                        throw new IllegalArgumentException();
                }
                return r;
            }
        };
        return new DefaultMustacheFactory(mr);
    }
}
