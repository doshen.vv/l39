package ru.inordic.jart.l28.dao;

import org.springframework.stereotype.Component;
import ru.inordic.jart.l28.pojo.Human;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 06.09.2021
 */
@Component()
//@Qualifier("second")
public class SecondHumanDaoImpl implements HumanDao{
    @Override
    public Human createNew(String snils, String name, String surname, int age, byte[] avatar) {
        return null;
    }

    @Override
    public Human getBySnils(String snils) throws IllegalArgumentException {
        return null;
    }

    @Override
    public void update(Human human) {

    }

    @Override
    public void deleteBySnils(String snils) throws IllegalArgumentException {

    }

    @Override
    public List<Human> getAll() {
        return null;
    }
}
