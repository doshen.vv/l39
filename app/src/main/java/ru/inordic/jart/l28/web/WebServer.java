package ru.inordic.jart.l28.web;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsServer;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inordic.jart.l28.pojo.Human;
import ru.inordic.jart.l28.service.HumanService;

//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author Artem R. Romanenko
 * @version 22.09.2021
 */
@Component
public class WebServer implements InitializingBean, DisposableBean {

    private HttpServer server;
    private CompletableFuture<Object> future;
    @Autowired
    private HumanService humanService;


    @Autowired
    private MustacheFactory mf;

    public WebServer() throws Exception {
        System.setProperty("http.keepAlive", "false");
        InetSocketAddress isa = new InetSocketAddress("localhost", 8080);
        server = HttpServer.create(isa, 0);
        future = new CompletableFuture<>();

        server.createContext("/stop", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                System.out.println("-------stop");
                OutputStream rb = he.getResponseBody();
                StringBuilder sb = new StringBuilder();
                sb.append("Server is stopped");

                byte[] hw = sb.toString().getBytes();
                he.sendResponseHeaders(200, hw.length);
                rb.write(hw);
                new Thread(() -> future.complete(null))
                        .start();
            }
        });

        server.createContext("/increment", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                System.out.println("increment log");
                OutputStream rb = he.getResponseBody();
                StringBuilder sb = new StringBuilder();
                URI uri = he.getRequestURI();
                String query = uri.getQuery();
                sb.append("Hello world ");
                if (query != null) {
                    String snils = null;
                    List<NameValuePair> params = URLEncodedUtils.parse(query, StandardCharsets.UTF_8);
                    for (NameValuePair p : params) {
                        switch (p.getName()) {
                            case "snils":
                                snils = p.getValue();
                                break;
                        }
                    }
                    humanService.incrementAgeBySnils(snils);
                    sb.append("ok");
                }
                byte[] hw = sb.toString().getBytes();
                he.sendResponseHeaders(200, hw.length);
                rb.write(hw);
            }
        });
        server.createContext("/change", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                System.out.println("create log");
                OutputStream rb = he.getResponseBody();
                StringBuilder sb = new StringBuilder();
                URI uri = he.getRequestURI();
                String query = uri.getQuery();
                sb.append("Hello world ");
                if (query != null) {
                    String snils = null;
                    String surname = "UNKNOWN";
                    List<NameValuePair> params = URLEncodedUtils.parse(query, StandardCharsets.UTF_8);
                    for (NameValuePair p : params) {
                        switch (p.getName()) {
                            case "snils":
                                snils = p.getValue();
                                break;
                            case "surname":
                                surname = p.getValue();
                                break;
                        }
                    }
                    humanService.changeSurname(surname, snils);
                    sb.append("ok");
                }
                byte[] hw = sb.toString().getBytes();
                he.sendResponseHeaders(200, hw.length);
                rb.write(hw);
            }
        });

    }


    public void awaitTermination() {
        try {
            future.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public void forceStop() {
        future.complete(null);
    }

    @Override
    public void destroy() throws Exception {
        server.stop(0);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        server.start();
    }
}
