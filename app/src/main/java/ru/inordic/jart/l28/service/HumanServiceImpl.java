package ru.inordic.jart.l28.service;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ru.inordic.jart.l28.dao.HumanDao;
import ru.inordic.jart.l28.event.MyLogEvent;
import ru.inordic.jart.l28.pojo.Human;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 01.09.2021
 */
@Component
public class HumanServiceImpl implements HumanService, DisposableBean, InitializingBean {


//

    //    @MyQual
    @Autowired
    @Qualifier("humanDaoImpl666")
    private HumanDao humanDao;

    @Autowired
    private ApplicationEventPublisher eventPublisher;
//    @Autowired
//    private NrdListener nrdListener;


//    @Autowired
//    private ApplicationContext ctx;
//
//    private Set<HumanDao> allHumanDaos;

//    //    @Autowired
//    public void init(HumanDao humanDao) {
//        this.humanDao = humanDao;
//    }

    @Override
    public Human createNew(String snils, String name, String surname, int age, byte[] avatar) {
//        nrdListener.increment();
        System.out.println("++++++publish from thread "+Thread.currentThread().getId());
        eventPublisher.publishEvent(new MyLogEvent(this));
        Human h = humanDao.createNew(snils, name, surname, age, avatar);

        return h;
    }

    @Override
    public Human findBySnils(String snils) {

        eventPublisher.publishEvent(new MyLogEvent(this));

        try {
            return humanDao.getBySnils(snils);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public void killBySnils(String snils) {

        eventPublisher.publishEvent(new MyLogEvent(this));
        humanDao.deleteBySnils(snils);
    }

    @Override
    public void incrementAgeBySnils(String snils) {

        eventPublisher.publishEvent(new MyLogEvent(this));
        Human h = humanDao.getBySnils(snils);
        h.setAge(h.getAge() + 1);
        humanDao.update(h);
    }

    @Override
    public void changeSurname(String surname, String snils) {

        eventPublisher.publishEvent(new MyLogEvent(this));
        Human h = humanDao.getBySnils(snils);
        h.setSurname(surname);
        humanDao.update(h);

    }



    @Override
    public void destroy() throws Exception {
        System.out.println("HumanService destroy");
    }

//    @PreDestroy
//    public void destructor() {
//        System.out.println("HumanService destructor");
//    }

    @Override
    public void afterPropertiesSet() throws Exception {
//        humanDao = ctx.getBean("humanDaoImpl666",HumanDao.class);
        System.out.println("HumanService afterPropertiesSet");
    }
//
//    @PostConstruct
//    public void postConstruct() {
//        System.out.println("HumanService postConstruct");
//    }

    @EventListener
    public void kfvsufiviusfuvi(ApplicationContextEvent event) {
        System.out.println("HumanService ApplicationContextEvent " + event.getClass());
    }

    @EventListener(ContextRefreshedEvent.class)
    public void kfvsufiviusfuvi2222(ContextRefreshedEvent event) {
        System.out.println("HumanService ContextRefreshedEvent");
    }

    @Override
    public List<Human> getAll() {
        return humanDao.getAll();
    }
}
