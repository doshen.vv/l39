package ru.inordic.jart.l28.dao;

import ru.inordic.jart.l28.pojo.Human;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 01.09.2021
 */
public interface HumanDao {


    Human createNew(String snils,
                    String name,
                    String surname,
                    int age, byte[] avatar);

    Human getBySnils(String snils)throws IllegalArgumentException;


    void update(Human human);

    void deleteBySnils(String snils) throws IllegalArgumentException;

    List<Human> getAll();


}
