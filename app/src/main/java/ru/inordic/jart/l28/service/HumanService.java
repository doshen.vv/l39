package ru.inordic.jart.l28.service;

import ru.inordic.jart.l28.pojo.Human;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 01.09.2021
 */
public interface HumanService {

    Human createNew(String snils,
                    String name,
                    String surname,
                    int age, byte[] avatar);

    Human findBySnils(String snils);

    void killBySnils(String snils);


    void incrementAgeBySnils(String snils);

    void changeSurname(String surname, String snils);

    List<Human> getAll();
}
