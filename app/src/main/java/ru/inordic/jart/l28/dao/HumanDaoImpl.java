package ru.inordic.jart.l28.dao;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import ru.inordic.jart.l28.pojo.Human;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 01.09.2021
 */
@Component("humanDaoImpl666")
//@Primary
//@Qualifier("primary")
// @MyQual
public class HumanDaoImpl implements HumanDao, AutoCloseable, ApplicationContextAware, BeanNameAware {

    private List<Human> storage = new ArrayList<>();
    private HashMap<String, Human> indexBySnils = new HashMap<>();


    @Override
    public void setBeanName(String name) {
        int a = 1;

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }


    @Override
    public Human createNew(String snils, String name, String surname, int age, byte[] avatar) {
        Human h = new Human();
        h.setSnils(snils);
        h.setAge(age);
        h.setName(name);
        h.setSurname(surname);
        h.setAvatar(avatar);
        storage.add(h);
        indexBySnils.put(snils, h);
        return h;
    }

    @Override
    public Human getBySnils(String snils) throws IllegalArgumentException {
        Human el = indexBySnils.get(snils);
        if (el == null) {
            throw new IllegalArgumentException("Human with snils " + snils + ": not found");
        }
        return el;

//        return storage.stream().filter(human -> human.getSnils().equals(snils)).findFirst()
//                .orElseThrow(
//                        () -> new IllegalArgumentException("Human with snils " + snils + ": not found"));

    }

    @Override
    public void update(Human human) {
        Human oldHuman = getBySnils(human.getSnils());
        oldHuman.setName(human.getName());
        oldHuman.setSurname(human.getSurname());
        oldHuman.setSnils(human.getSnils());

        save(oldHuman);
    }


    @Override
    public void deleteBySnils(String snils) throws IllegalArgumentException {
        Human oldHuman = getBySnils(snils);
        indexBySnils.remove(snils);
        storage.remove(oldHuman);
    }

    @Override
    public List<Human> getAll() {
        return new ArrayList<>(storage);
    }

    private void save(Human human) {
        //TODO не забыть реализовать

    }

    @Override
    public void close() throws Exception {
        System.out.println("HumanDao close");
    }


}
